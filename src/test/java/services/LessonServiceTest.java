package services;

import dao.LessonDao;
import model.Group;
import model.Lesson;
import model.Student;
import model.Visit;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/* В домашнем задании для покрытия сервиса тестами, необходимо было использовать
   предложенный Михаилом проект из его репозитария.
   Но поскольку для выполнения дз я решил использовать свой проект из прошлого задания,
   в нем сервисов гораздо больше. Покрыть их все я, к сожалению, не успел,
   но в будущем буду стараться покрывать все. */

public class LessonServiceTest {
    private LessonService lessonService;
    @Mock private LessonDao lessonDao;
    @Mock private Lesson lesson;
    @Mock private Visit visit;
    @Mock private Student student;
    @Mock private List<Visit> visitList;
    @Mock private List<Lesson> lessonList;
    @Mock private Group group;

    @Before
    public void setUp() {
        initMocks(this);
        this.lessonService = new LessonService(lessonDao);
    }

    @Test
    public void createLessonDaoReturnsNull() {
        Lesson lesson;
        when(lessonDao.getAllLessons()).thenReturn(null);
        lesson = lessonService.createLesson("test", null);
        assertNotNull(lesson);
    }

    @Test
    public void createLessonDaoReturnsLesson() {
        Lesson lesson;
        when(lessonDao.getAllLessons()).thenReturn(lessonList);
        lesson = lessonService.createLesson("test", null);
        assertNotNull(lesson);
    }

    @Test
    public void getLessonDaoReturnsNull() {
        when(lessonDao.getLesson(anyInt())).thenReturn(null);
        Lesson testLesson = lessonService.getLesson(-1);
        assertNull(testLesson);
    }

    @Test
    public void getLessonDaoReturnsLesson() {
        when(lessonDao.getLesson(anyInt())).thenReturn(lesson);
        Lesson testLesson = lessonService.getLesson(1);
        assertNotNull(testLesson);
    }

    @Test
    public void getAllLessonsReturnsNull() {
        when(lessonDao.getAllLessons()).thenReturn(null);
        List<Lesson> testList = lessonService.getAllLessons();
        assertNull(testList);
    }

    @Test
    public void getAllLessonsReturnsLessonList() {
        when(lessonDao.getAllLessons()).thenReturn(lessonList);
        List<Lesson> testList = lessonService.getAllLessons();
        assertNotNull(testList);
    }

    @Test
    public void addVisitDaoReturnsNull() {
        when(lessonDao.getLesson(anyInt())).thenReturn(null);
        lessonService.addVisit(1, new Visit(1, null, 5));
        assertEquals(0, lesson.getVisits().size());
    }

    @Test
    public void addVisitDaoReturnsLesson() {
        lesson = new Lesson(1, "test", null);
        when(lessonDao.getLesson(1)).thenReturn(lesson);
        lessonService.addVisit(1, new Visit(1, null, 5));
        assertEquals(1, lesson.getVisits().size());
    }

    @Test
    public void getAvgGradeDaoReturnsNull() {
        when(lessonDao.getLesson(anyInt())).thenReturn(null);
        Double result = lessonService.getAvgGrade(anyInt());
        assertEquals(0.0d, result);
    }

    @Test
    public void getAvgGradeGetVisitsReturnsNull() {
        when(lessonDao.getLesson(anyInt())).thenReturn(lesson);
        when(lesson.getVisits()).thenReturn(null);
        Double result = lessonService.getAvgGrade(anyInt());
        assertEquals(0.0d, result);
    }

    @Test
    public void getAvgGradeGetVisitsReturnsEmpty() {
        when(lessonDao.getLesson(anyInt())).thenReturn(lesson);
        when(lesson.getVisits()).thenReturn(visitList);
        Double result = lessonService.getAvgGrade(anyInt());
        assertEquals(0.0d, result);
    }

    @Test
    public void getAvgGradeGetVisitsReturnsVisitList() {
        when(lessonDao.getLesson(anyInt())).thenReturn(lesson);
        when(lesson.getVisits()).thenReturn(visitList);
        visitList = new ArrayList<Visit>();
        visit = new Visit(1, student, null);
        visitList.add(visit);
        when(lesson.getVisits()).thenReturn(visitList);
        Double result = lessonService.getAvgGrade(anyInt());
        assertEquals(0.0d, result);
    }

}
