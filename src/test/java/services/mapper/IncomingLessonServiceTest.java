package services.mapper;

import model.Group;
import model.Lesson;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import outermodel.IncomingLesson;
import static junit.framework.TestCase.*;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class IncomingLessonServiceTest {
    public IncomingLessonService incomingLessonService;
    @Mock IncomingLesson incomingLesson;
    @Mock IncomingLessonMapper incomingLessonMapper;
    @Mock Lesson lesson;

    @Before
    public void setUp() {
        initMocks(this);
        this.incomingLessonService = new IncomingLessonService(new IncomingLessonImpl());
    }

    @Test
    public void createIncomingLessonReturnsNull() {
        // если mapIncomingLesson вернет null, Lesson должен быть тоже null
        when(incomingLessonMapper.mapIncomingLesson(incomingLesson)).thenReturn(null);
        Lesson newLesson = incomingLessonService.createIncomingLesson(incomingLesson);
        assertNull(newLesson);
    }

    @Test
    public void createIncomingLessonReturnsLesson() {
        // если mapIncomingLesson вернет lesson, то и метод вернет lesson
        lesson = new Lesson(1, "Some Topic", new Group(1, "Some Group"));
        when(incomingLessonMapper.mapIncomingLesson(incomingLesson)).thenReturn(lesson);
        Lesson newLesson = incomingLessonMapper.mapIncomingLesson(incomingLesson);
        assertNotNull(newLesson);
    }
}
