package services.mapper;

import model.Group;
import model.Lesson;
import model.Student;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import outermodel.IncomingLesson;
import services.StudentService;
import services.VisitService;

import static junit.framework.TestCase.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class IncomingLessonImplTest {
    private IncomingLessonImpl incomingLessonImpl;
    @Mock private IncomingLesson incomingLesson;
    @Mock private Group group;
    @Mock private StudentService studentService;
    @Mock private Student student;
    @Mock private VisitService visitService;

    @Before
    public void setUp() {
        initMocks(this);
        this.incomingLessonImpl = new IncomingLessonImpl();
    }

    @Test
    public void mapIncomingLessonGetGroupReturnsNull() {
        // если объект group в incomingLesson вернет null, метод должен вернуть null
        when(incomingLesson.getGroup()).thenReturn(null);
        Lesson newLesson = incomingLessonImpl.mapIncomingLesson(incomingLesson);
        assertNull(newLesson);
    }

    @Test
    public void mapIncomingLessonGetTopicReturnsNull() {
        // если incomingLesson.gettopic вернет null, метод должен вернуть null
        when(incomingLesson.getGroup()).thenReturn(group);
        when(incomingLesson.getTopic()).thenReturn(null);
        Lesson newLesson = incomingLessonImpl.mapIncomingLesson(incomingLesson);
        assertNull(newLesson);
    }

    @Test
    public void mapIncomingLessonCreateStudentReturnsNull() {
        // если studentService.createStudent вернет null, метод должен вернуть Lesson
        when(incomingLesson.getGroup()).thenReturn(group);
        when(incomingLesson.getTopic()).thenReturn("Some topic");
        when(studentService.createStudent(anyInt(), anyString(), anyString(), anyString())).thenReturn(null);
        Lesson newLesson = incomingLessonImpl.mapIncomingLesson(incomingLesson);
        assertNotNull(newLesson);
        assertNotNull(newLesson.getVisits());
    }

    @Test
    public void mapIncomingLessonCreateVisitReturnsNull() {
        // если visitService.createVisit вернет null, метод должен вернуть Lesson
        when(incomingLesson.getGroup()).thenReturn(group);
        when(incomingLesson.getTopic()).thenReturn("Some topic");
        when(studentService.createStudent(anyInt(), anyString(), anyString(), anyString())).thenReturn(student);
        when(incomingLesson.getGrade()).thenReturn(5);
        when(visitService.createVisit(student, 5)).thenReturn(null);
        Lesson newLesson = incomingLessonImpl.mapIncomingLesson(incomingLesson);
        assertNotNull(newLesson);
        assertNotNull(newLesson.getVisits());
    }
}
