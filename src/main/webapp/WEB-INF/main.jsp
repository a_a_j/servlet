<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Main</title>
    </head>
    <body>
        <h1>Main Page</h1>
        <h3>Hello, <%=request.getSession().getAttribute("login")%>! <a href="/logout">[Logout]</a></h3>
        <table>
            <tr><th colspan="2" align="left">Groups:</th></tr>
            <c:forEach items="${list}" var="group">
                <tr>
                    <td><a href="/group?id=${group.id}">${group.name}</a></td>
                </tr>
            </c:forEach>
        </table>
        <br>
        <form action="/add_group" method="post">
            <table>
                <tr><th colspan="2" align="left">Add new group:</th></tr>
                <tr>
                    <td>Group name</td>
                    <td><input type="text" name="group_name"></td>
                </tr>
                <tr>
                    <td colspan="2" align="center"><input type="submit" value="Add"></td>
                </tr>
            </table>
        </form>
    </body>
</html>