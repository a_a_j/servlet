<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <title>Group View</title>
    </head>
<body>
    <h1><%=request.getAttribute("groupName")%></h1>
    <p><a href="/main">Go Back</a></p>
    <table>
        <tr>
            <th colspan="2" align="left">${studentsGroup.size() > 0 ? 'Students:' : 'Group is empty'}</th>
        </tr>
        <c:forEach items="${studentsGroup}" var="student">
            <tr>
                <td>${student.name} ${student.lastName}</td>
                <td><a href="/delete_student?id=${student.id}">[delete]</a></td>
            </tr>
        </c:forEach>
    </table>
    <br>
    <form action="/add_student" method="post">
        <table>
            <tr>
                <th colspan="2" align="left">Add new student:</th>
            </tr>
            <tr>
                <td>Name</td>
                <td><input type="text" name="name"></td>
            </tr>
            <tr>
                <td>Last name</td>
                <td><input type="text" name="last_name"></td>
            </tr>
            <tr>
                <td>City</td>
                <td><input type="text" name="city"></td>
            </tr>
            <tr>
                <td colspan="2" align="center"><input type="submit" value="Add"></td>
            </tr>
        </table>
    </form>
</body>
</html>