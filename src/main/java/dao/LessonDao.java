package dao;

import model.Lesson;
import model.Visit;

import java.util.List;

public interface LessonDao {
    void addLesson(Lesson lesson);

    Lesson getLesson(int id);

    List<Lesson> getAllLessons();
}
