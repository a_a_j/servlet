package dao;

import model.Group;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupImpl implements GroupDao {
    Map<Integer, Group> groups = new HashMap();

    public GroupImpl() {
        groups.put(1, new Group(1, "Math group"));
        groups.put(2, new Group(2, "IT group"));
        groups.put(3, new Group(3, "Linguistic group"));
    }

    public void addGroup(Group group) {
        groups.put(group.getId(), group);
    }

    public void deleteGroup(int id) {
        groups.remove(id);
    }

    public void updateGroup(int id, String name) {
        Group newGroup = new Group(id, name);
        groups.put(id, newGroup);
    }

    public Group getGroup(int id) {
        return groups.get(id);
    }

    public List<Group> getAllGroups() {
        return new ArrayList<Group>(groups.values());
    }
}
