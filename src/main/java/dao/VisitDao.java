package dao;

import model.Visit;

import java.util.List;

public interface VisitDao {
    void addVisit(Visit visit);

    Visit getVisit(int id);

    List<Visit> getAllVisits();
}
