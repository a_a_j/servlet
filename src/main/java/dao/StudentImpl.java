package dao;

import model.Lesson;
import model.Student;
import model.Visit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StudentImpl implements StudentDao {

    Map<Integer, Student> students = new HashMap();

    public StudentImpl() {
        students.put(1, new Student(1,2, "Alexander", "Zharikov", "Moscow"));
        students.put(2, new Student(2,2, "Eugeniy", "Botylev", "Moscow"));
        students.put(3, new Student(3,2, "Anvar", "Palvanov", "Moscow"));
        students.put(4, new Student(4,1, "Grigory", "Perelman", "Moscow"));
        students.put(5, new Student(5,3, "Ivan", "Ivanov", "Moscow"));
    }

    public void addStudent(Student student) {
        students.put(student.getId(), student);
    }

    public void deleteStudent(int id) {
        students.remove(id);
    }

    public Student getStudent(int id) {
        return null;
    }

    public List<Student> getAllStudents() {
        return new ArrayList(students.values());
    }
}
