package dao;

import model.Group;
import model.Lesson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LessonImpl implements LessonDao {
    Map<Integer, Lesson> lessons = new HashMap();
    Group group;

    public LessonImpl() {
        Group group = new Group(2, "IT group");
        lessons.put(1, new Lesson(1, "Java", group));
        lessons.put(2, new Lesson(2, "Postgres", group));
        lessons.put(3, new Lesson(3, "Oracle", group));
    }

    public LessonImpl(Map<Integer, Lesson> lessons, Group group) {
        this.lessons = lessons;
        this.group = group;
    }

    public void addLesson(Lesson lesson) {
        lessons.put(lessons.size()+1, lesson);
    }

    public Lesson getLesson(int id) {
        return lessons.get(id);
    }

    public List<Lesson> getAllLessons() {
        return new ArrayList<Lesson>(lessons.values());
    }
}
