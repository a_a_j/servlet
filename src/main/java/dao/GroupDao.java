package dao;

import model.Group;

import java.util.List;

public interface GroupDao {
    void addGroup(Group group);

    void deleteGroup(int id);

    void updateGroup(int id, String name);

    Group getGroup(int id);

    List<Group> getAllGroups();
}
