package dao;

import model.Student;
import model.Visit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VisitImpl implements VisitDao {
    Map<Integer, Visit> visits = new HashMap();
    int id;
    Student student;
    int grade;

    public VisitImpl() {
        Student student = new Student(1,2,"Alexander", "Petrov", "Moscow");
        visits.put(1, (new Visit(1, student, 4)));
        student = new Student(2,2,"Ivan", "Ivanov", "Moscow");
        visits.put(2, (new Visit(2,student, 3)));
        student = new Student(3,2,"Boris", "Sidorov", "Moscow");
        visits.put(3, (new Visit(3, student, 3)));
    }

    public VisitImpl(int id, Student student, int grade) {
        visits.put(id, (new Visit(id, student, grade)));
    }

    public void addVisit(Visit visit) {
        visits.put(visit.getId(), visit);
    }

    public Visit getVisit(int id) {
        return visits.get(id);
    }

    public List<Visit> getAllVisits() {
        return new ArrayList<Visit>(visits.values());
    }
}
