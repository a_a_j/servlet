package dao;

import model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserImpl implements UserDao {
    Map<Integer, User> users = new HashMap();

    public UserImpl() {
        users.put(1, new User(1, "admin", "admin"));
        users.put(2, new User(2, "alex", "123"));
    }

    public void addUser(User user) {
        users.put(user.getId(), user);
    }

    public void deleteUser(int id) {
        users.remove(id);
    }

    public User getUser(int id) {
        return users.get(id);
    }

    public List<User> getAllUsers() {
        return new ArrayList(users.values());
    }
}
