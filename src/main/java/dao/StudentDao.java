package dao;

import model.Lesson;
import model.Student;
import model.Visit;

import java.util.List;

public interface StudentDao {
    void addStudent(Student student);

    void deleteStudent(int id);

    Student getStudent(int id);

    List<Student> getAllStudents();

}
