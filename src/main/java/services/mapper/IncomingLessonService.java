package services.mapper;

import model.Lesson;
import outermodel.IncomingLesson;

// сервис для маппера
public class IncomingLessonService {
    IncomingLessonMapper incomingLessonMapper;

    public IncomingLessonService(IncomingLessonMapper incomingLessonMapper) {
        this.incomingLessonMapper = incomingLessonMapper;
    }

    public Lesson createIncomingLesson(IncomingLesson incomingLesson) {
        Lesson lesson = incomingLessonMapper.mapIncomingLesson(incomingLesson);
        return lesson;
    }
}
