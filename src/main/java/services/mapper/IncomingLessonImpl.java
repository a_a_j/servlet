package services.mapper;

import dao.LessonImpl;
import dao.StudentImpl;
import dao.VisitImpl;
import model.Group;
import model.Lesson;
import model.Student;
import model.Visit;
import outermodel.IncomingLesson;
import services.LessonService;
import services.StudentService;
import services.VisitService;

import java.util.HashMap;
import java.util.Map;

public class IncomingLessonImpl implements IncomingLessonMapper {

    Map<Integer, IncomingLesson> incomingLessons = new HashMap();

    public IncomingLessonImpl() {
    }

    /* Реализация маппера
       Метод "разбирает" входящий объект incomingLesson
       и возвращает лекцию, внутри которой посещение со студентом */
    public Lesson mapIncomingLesson(IncomingLesson incomingLesson) {
        StudentService studentService = new StudentService(new StudentImpl());
        Group group = incomingLesson.getGroup();
        Lesson lesson = null;
        if (group != null) {
            LessonService lessonService = new LessonService(new LessonImpl());
            lesson = lessonService.createLesson(incomingLesson.getTopic(), group);
            if (lesson != null) {
                VisitService visitService = new VisitService(new VisitImpl());
                Student student = studentService.createStudent(group.getId(), incomingLesson.getName(),
                                                               incomingLesson.getLastName(), null);
                Visit visit = visitService.createVisit(student, incomingLesson.getGrade());
                lesson.addVisit(visit);
            }
        }
        return lesson;
    }
}
