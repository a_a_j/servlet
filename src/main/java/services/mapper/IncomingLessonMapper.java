package services.mapper;

import model.Lesson;
import outermodel.IncomingLesson;

import java.util.List;

public interface IncomingLessonMapper {
    Lesson mapIncomingLesson(IncomingLesson incomingLesson);
}
