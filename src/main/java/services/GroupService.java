package services;

import model.Group;
import dao.GroupDao;
import java.util.List;

public class GroupService {
    private GroupDao groupDao;

    public GroupService(GroupDao groupDao) {
        this.groupDao = groupDao;
    }

    public void createGroup(String name) {
        int id = groupDao.getAllGroups().size() + 1;
        Group newGroup = new Group(id, name);
        groupDao.addGroup(newGroup);
    }

    public Group getGroup(int id) {
        return groupDao.getGroup(id);
    }

    public List<Group> getAllGroups() {
        return groupDao.getAllGroups();
    }
}
