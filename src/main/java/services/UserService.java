package services;

import model.User;
import dao.UserDao;

public class UserService {
    private UserDao userDao;

    public UserService(UserDao userDao) {
        this.userDao = userDao;
    }

   public Boolean UserIsValid(String login, String password){
       for (User user: userDao.getAllUsers()) {
           if (((user.getLogin() != null) &&
                (user.getLogin().equals(login))) &&
               ((user.getPassword() != null) &&
                (user.getPassword().equals(password)))) {
               return true;
           }
       }
       return false;
    }
}
