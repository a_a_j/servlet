package services;

import dao.VisitDao;
import model.Student;
import model.Visit;

public class VisitService {
    private VisitDao visitDao;

    public VisitService(VisitDao visitDao) {
        this.visitDao = visitDao;
    }

    public Visit createVisit(Student student, int grade) {
        Visit visit = null;
        if (student != null) {
            int id = visitDao.getAllVisits().size() + 1;
            visit = new Visit(id, student, grade);
            visitDao.addVisit(visit);
        }
        return visit;
    }
}
