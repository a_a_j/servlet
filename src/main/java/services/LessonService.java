package services;

import dao.LessonDao;
import model.Group;
import model.Lesson;
import model.Visit;

import java.util.List;

public class LessonService {
    private LessonDao lessonDao;

    public LessonService(LessonDao lessonDao) {
        this.lessonDao = lessonDao;
    }

    public Lesson createLesson(String topic, Group group) {
        Lesson newLesson = null;
        if (topic != null) {
            List<Lesson> allLessons = lessonDao.getAllLessons();
            int id = 1;
            if (allLessons != null) {
                id = lessonDao.getAllLessons().size() + 1;
            }
            newLesson = new Lesson(id, topic, group);
            lessonDao.addLesson(newLesson);
        }
        return newLesson;
    }

    public Lesson getLesson(int id) {
        return lessonDao.getLesson(id);
    }

    public List<Lesson> getAllLessons() {
        return lessonDao.getAllLessons();
    }

    public void addVisit(int idLesson, Visit visit) {
        Lesson lesson = lessonDao.getLesson(idLesson);
        if (lesson != null) {
            lesson.addVisit(visit);
        }
    }

    public Double getAvgGrade(int idLesson) {
        Double avgGrade = 0.0;
        Lesson lesson = lessonDao.getLesson(idLesson);
        if (lesson != null) {
            List<Visit> visits = lesson.getVisits();
            if (visits != null) {
                int visitsSize = visits.size();
                if (visitsSize > 0) {
                    for (Visit v : visits) {
                        Integer visitGrade = v.getGrade();
                        avgGrade += (visitGrade != null ? visitGrade : 0);
                    }
                    avgGrade = avgGrade / visitsSize;
                }
            }
        }
        return avgGrade;
    }
}
