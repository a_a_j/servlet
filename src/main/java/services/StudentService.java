package services;

import model.Student;
import dao.StudentDao;

import java.util.ArrayList;
import java.util.List;

public class StudentService {
    private StudentDao studentDao;

    public StudentService(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public Student createStudent(int idGroup, String name, String lastName, String city) {
        int id = studentDao.getAllStudents().size() + 1; //TODO: fix it!
        Student newStudent = new Student(id, idGroup, name, lastName, city);
        studentDao.addStudent(newStudent);
        return newStudent;
    }

    public void deleteStudent(int id) {
        studentDao.deleteStudent(id);
    }

    public Student getStudentById (int id) {
        return studentDao.getStudent(id);
    }

    public List<Student> getStudentsByGroup (int idGroup) {
        List<Student> groupStudents = new ArrayList();
        for(Student s : studentDao.getAllStudents()) {
            if(s.getIdGroup() == idGroup) {
                groupStudents.add(s);
            }
        }
        return groupStudents;
    }

    public List<Student> getStudentsByVowel() {
        final String vowelsRegex = "^[aeiou].*";
        List<Student> studentList = new ArrayList();
        for (Student s : studentDao.getAllStudents()) {
            if (s.getLastName().toLowerCase().matches(vowelsRegex)) {
                studentList.add(s);
            }
        }
        return studentList;
    }

}
