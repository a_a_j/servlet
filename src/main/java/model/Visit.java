package model;

public class Visit {
    private int id;
    private Student student;
    private Integer grade;

    public Visit() {
    }

    public Visit(int id, Student student, Integer grade) {
        this.id = id;
        this.student = student;
        this.grade = grade;
    }

    public int getId() {
        return id;
    }

    public Integer getGrade() {
        return grade;
    }
}
