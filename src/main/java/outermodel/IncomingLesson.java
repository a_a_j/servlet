package outermodel;

import model.Group;

public class IncomingLesson {
    private Integer id;
    private String name;
    private String lastName;
    private String groupName;
    private String topic;
    private Group group;
    private Integer grade;

    public IncomingLesson() {
    }

    public IncomingLesson(Integer id, String name, String lastName, String groupName, String topic, Group group, Integer grade) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.groupName = groupName;
        this.topic = topic;
        this.group = group;
        this.grade = grade;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGroupName() {
        return groupName;
    }

    public String getTopic() {
        return topic;
    }

    public Group getGroup() {
        return group;
    }

    public Integer getGrade() {
        return grade;
    }
}
