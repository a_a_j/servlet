import dao.LessonImpl;
import dao.StudentImpl;
import model.Visit;
import services.LessonService;
import services.StudentService;

public class Main {
    public static void main(String[] args) {
        StudentService studentService = new StudentService(new StudentImpl());
        LessonService lessonService = new LessonService(new LessonImpl());
        // добавляем посещения в лекцию
        lessonService.addVisit(1, new Visit(1, studentService.getStudentById(1), 3));
        lessonService.addVisit(1, new Visit(2, studentService.getStudentById(2), 5));
        lessonService.addVisit(1, new Visit(3, studentService.getStudentById(3), 5));

        /* Задания 1. Сервис, возвращающий среднюю оценку за занятие (на вход id занятия).
           В дз нет строгого условия в каком сервисе необходимо реализовать метод,
           поэтому я выбрал lessonService, т.к. на мой взгляд это наиболее логично. */
        System.out.println(lessonService.getAvgGrade(1));
        /* Задание 2. Метод, возвращающий список студентов, у которых фамилии начинаются на гласную букву.
           В данном случае больше подходит studentService. */
        System.out.println(studentService.getStudentsByVowel().toString());
    }
}
