package controller;

import model.Group;
import dao.StudentImpl;
import services.GroupService;
import services.StudentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GroupServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GroupService groupService = (GroupService) req.getSession().getAttribute("groupService");
        if (groupService != null) {
            Integer idGroup = Integer.valueOf(req.getParameter("id"));
            if (idGroup == 0) {
                idGroup = (Integer) req.getSession().getAttribute("idGroup");
            }
            Group group = groupService.getGroup(idGroup);
            if (group != null) {
                StudentService studentService = (StudentService) req.getSession().getAttribute("studentService");
                if (studentService == null) {
                    studentService = new StudentService(new StudentImpl());
                }
                req.getSession().setAttribute("studentService", studentService);
                req.getSession().setAttribute("idGroup", idGroup);
                req.setAttribute("studentsGroup", studentService.getStudentsByGroup(idGroup));
                req.setAttribute("groupName", group.getName());
                req.getRequestDispatcher("/WEB-INF/group.jsp").forward(req, resp);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
