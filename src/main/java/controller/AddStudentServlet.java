package controller;

import services.StudentService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddStudentServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StudentService studentService = (StudentService) req.getSession().getAttribute("studentService");
        int idGroup = (Integer) req.getSession().getAttribute("idGroup");
        String name = req.getParameter("name");
        String lastName = req.getParameter("last_name");
        String city = req.getParameter("city");
        if (studentService != null && name != null) {
            studentService.createStudent(idGroup, name, lastName, city);
            req.getSession().setAttribute("studentService", studentService);
        }
        if (idGroup > 0) {
            req.getRequestDispatcher("/group?id=" + idGroup).forward(req, resp);
        } else {
            req.getRequestDispatcher("/main").forward(req, resp);
        }
    }
}
