package controller;

import services.GroupService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AddGroupServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GroupService groupService = (GroupService) req.getSession().getAttribute("groupService");
        if (groupService != null) {
            String groupName = req.getParameter("group_name");
            groupService.createGroup(groupName);
            req.setAttribute("groupService", groupService);
            req.getRequestDispatcher("/main").forward(req, resp);
        } else {
            req.getRequestDispatcher("/index.jsp").forward(req, resp);
        }
    }
}
