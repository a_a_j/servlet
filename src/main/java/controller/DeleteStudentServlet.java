package controller;

import services.StudentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteStudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Integer idGroup = (Integer) req.getSession().getAttribute("idGroup");
        int idStudent = Integer.parseInt(req.getParameter("id"));
        StudentService studentService = (StudentService) req.getSession().getAttribute("studentService");
        if (studentService != null && idStudent > 0) {
            studentService.deleteStudent(idStudent);
        }
        if (idGroup > 0) {
            req.getRequestDispatcher("/group?id=" + idGroup).forward(req, resp);
        } else {
            req.getRequestDispatcher("/main").forward(req, resp);
        }
    }
}
