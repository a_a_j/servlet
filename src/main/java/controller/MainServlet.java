package controller;

import dao.GroupImpl;
import services.GroupService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MainServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = (String) request.getSession().getAttribute("login");
        if (login != null) {
            GroupService groupService = (GroupService) request.getAttribute("groupService");
            if (groupService == null) {
                groupService = new GroupService(new GroupImpl());
            }
            request.getSession().setAttribute("groupService", groupService);
            request.setAttribute("list", groupService.getAllGroups());
            request.getRequestDispatcher("/WEB-INF/main.jsp").forward(request, response);
        } else {
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
